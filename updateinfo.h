#ifndef UPDATEINFO_H
#define UPDATEINFO_H


class updateinfo
{
public:
	updateinfo(const double iheightpx=0, const double icamerapos=0, const double iwidhtunits=0);
	updateinfo(const double info[3]);
	updateinfo(const updateinfo &info);
	updateinfo operator=(const updateinfo &info);
	float heightpx=0;
	float camerapos=0;
	float widhtunits=0;

};

#endif // UPDATEINFO_H
