QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    entities/baseentity.cpp \
    entities/basegunnerentity.cpp \
    entities/blockentity.cpp \
    entities/bulletentity.cpp \
    entities/fireentity.cpp \
    entities/flameentity.cpp \
    entities/leverentity.cpp \
    entities/playerentity.cpp \
    entities/sentryentity.cpp \
    entities/smithentity.cpp \
    entities/soupentity.cpp \
    entities/textentity.cpp \
    entities/textureobject.cpp \
    gamewidget.cpp \
    levels/baselevel.cpp \
    levels/level_0.cpp \
    levels/level_1.cpp \
    levels/level_2.cpp \
    levels/level_3.cpp \
    levels/level_4.cpp \
    levels/level_5.cpp \
    levels/level_6.cpp \
    levels/level_7.cpp \
    main.cpp \
    mainwindow.cpp \
    menus/basemenuscene.cpp \
    menus/creditscene.cpp \
    menus/levelmenu.cpp \
    menus/mainmenuscene.cpp \
    menuwidget.cpp \
    qcustomtextbox.cpp \
    updateinfo.cpp

HEADERS += \
    defines.h \
    entities/baseentity.h \
    entities/basegunnerentity.h \
    entities/blockentity.h \
    entities/bulletentity.h \
    entities/fireentity.h \
    entities/flameentity.h \
    entities/leverentity.h \
    entities/playerentity.h \
    entities/sentryentity.h \
    entities/smithentity.h \
    entities/soupentity.h \
    entities/textentity.h \
    entities/textureobject.h \
    gamewidget.h \
    levels/baselevel.h \
    levels/level_0.h \
    levels/level_2.h \
    levels/level_3.h \
    levels/level_4.h \
    levels/level_5.h \
    levels/level_1.h \
    levels/level_6.h \
    levels/level_7.h \
    mainwindow.h \
    menus/basemenuscene.h \
    menus/creditscene.h \
    menus/levelmenu.h \
    menus/mainmenuscene.h \
    menuwidget.h \
    qcustomtextbox.h \
    updateinfo.h

FORMS += \
    mainwindow.ui

TRANSLATIONS += \
    borderworld_en_AT.ts
CONFIG += lrelease
CONFIG += embed_translations

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    recources/entity-file.qrc
