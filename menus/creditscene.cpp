#include "creditscene.h"

creditScene::creditScene()
{
	setBackgroundBrush(QBrush(Qt::black));

	textNames=new QCustomTextBox("Florian Moser: Level Design & Textures\nFlorian Plasun: Game Engine & Story");

	textExit=new QCustomTextBox("EXIT");

	textNames->setDefaultTextColor(Qt::white);

	textExit->setDefaultTextColor(Qt::white);

	addItem(textNames);

	addItem(textExit);
}

creditScene::~creditScene()
{
	delete textNames;
	delete textExit;
}
void creditScene::resize()
{
	int height= this->height();
	int width= this->width();

	textNames->setFont(QFont("Liberation Serif", height/20, 990, false));
	textNames->adjustSize();
	textNames->setPos(width/2.0-(textNames->textWidth()/2.0), height/5-height/32);



	textExit->setFont(QFont("Liberation Serif", height/16, 990, false));
	textExit->adjustSize();
	textExit->setPos(width/2.0-(textExit->textWidth()/2.0), 4*height/5-height/32);
}


