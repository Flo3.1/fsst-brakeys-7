#include "mainmenuscene.h"

mainMenuScene::mainMenuScene(QObject *parent) : baseMenuScene(parent)
{

	setBackgroundBrush(QBrush(Qt::black));

	textPlay=new QCustomTextBox("PLAY");
	textLevels=new QCustomTextBox("LEVELS");
	textCredits=new QCustomTextBox("CREDITS");
	textExit=new QCustomTextBox("EXIT");

	textPlay->setDefaultTextColor(Qt::white);
	textLevels->setDefaultTextColor(Qt::white);
	textCredits->setDefaultTextColor(Qt::white);
	textExit->setDefaultTextColor(Qt::white);

	addItem(textPlay);
	addItem(textLevels);
	addItem(textCredits);
	addItem(textExit);
}

mainMenuScene::~mainMenuScene()
{
	delete textPlay;
	delete textLevels;
	delete textCredits;
	delete textExit;
}

void mainMenuScene::resize()
{
	int height= this->height();
	int width= this->width();

	textPlay->setFont(QFont("Liberation Serif", height/16, 99, false));
	textPlay->adjustSize();
	textPlay->setPos(width/2.0-(textPlay->textWidth()/2.0), height/5-height/32);

	textLevels->setFont(QFont("Liberation Serif", height/16, 99, false));
	textLevels->adjustSize();
	textLevels->setPos(width/2.0-(textLevels->textWidth()/2.0), 2*height/5-height/32);


	textCredits->setFont(QFont("Liberation Serif", height/16, 99, false));
	textCredits->adjustSize();
	textCredits->setPos(width/2.0-(textCredits->textWidth()/2.0), 3*height/5-height/32);


	textExit->setFont(QFont("Liberation Serif", height/16, 99, false));
	textExit->adjustSize();
	textExit->setPos(width/2.0-(textExit->textWidth()/2.0), 4*height/5-height/32);
}
