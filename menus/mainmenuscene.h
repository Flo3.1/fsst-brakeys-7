#ifndef MAINMENUSCENE_H
#define MAINMENUSCENE_H

#include "menus/basemenuscene.h"
#include <QObject>
#include "qcustomtextbox.h"

class mainMenuScene : public baseMenuScene
{
	Q_OBJECT
public:
	explicit mainMenuScene(QObject *parent = nullptr);
	~mainMenuScene();
	void resize();
	// the following are public so the Graphics viev can contect their signals
	QCustomTextBox * textPlay, *textLevels, *textCredits, *textExit;

};

#endif // MAINMENUSCENE_H
