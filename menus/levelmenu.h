#ifndef LEVELMENU_H
#define LEVELMENU_H

#include "basemenuscene.h"
#include "../qcustomtextbox.h"

class levelMenu : public baseMenuScene
{
	Q_OBJECT
public:
	explicit levelMenu(QObject *parent = nullptr);

	~levelMenu();
	void resize();
	QCustomTextBox *textLevel[6], *textExit;
};

#endif // LEVELMENU_H
