#include "levelmenu.h"

levelMenu::levelMenu(QObject *parent) : baseMenuScene(parent)
{


	setBackgroundBrush(QBrush(Qt::black));
	for(int i=0; i<6; i++){
		textLevel[i]=new QCustomTextBox("LEVEL "+QString::number(i+1));
		textLevel[i]->setDefaultTextColor(Qt::white);
		addItem(textLevel[i]);
	}
	textExit=new QCustomTextBox("EXIT");

	textExit->setDefaultTextColor(Qt::white);

	addItem(textExit);
}

levelMenu::~levelMenu()
{
	for(int i=0; i<6; i++){
		delete textLevel[i];
	}
	delete textExit;
}

void levelMenu::resize()
{
	int height= this->height();
	int width= this->width();

	for(int i=0; i<6; i++){
		textLevel[i]->setFont(QFont("Liberation Serif", height/32, 99, false));
		textLevel[i]->adjustSize();
		textLevel[i]->setPos(width/2.0-(textLevel[i]->textWidth()/2.0), height/9*i+height/16);
	}


	textExit->setFont(QFont("Liberation Serif", height/32, 99, false));
	textExit->adjustSize();
	textExit->setPos(width/2.0-(textExit->textWidth()/2.0), 4*height/5-height/32);
}
