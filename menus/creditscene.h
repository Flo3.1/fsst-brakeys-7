#ifndef CREDITSCENE_H
#define CREDITSCENE_H

#include "basemenuscene.h"
#include "../qcustomtextbox.h"

class creditScene : public baseMenuScene
{
	Q_OBJECT
public:
	creditScene();
	~creditScene();
	void resize();
	QCustomTextBox * textNames, *textExit;
};

#endif // CREDITSCENE_H
