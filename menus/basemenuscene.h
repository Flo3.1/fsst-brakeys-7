#ifndef BASEMENUSCENE_H
#define BASEMENUSCENE_H

#include <QGraphicsScene>

class baseMenuScene : public QGraphicsScene
{
	Q_OBJECT
public:
	explicit baseMenuScene(QObject *parent = nullptr);

	virtual void resize(){};
};

#endif // BASEMENUSCENE_H
