#ifndef QCUSTOMTEXTBOX_H
#define QCUSTOMTEXTBOX_H

#include <QGraphicsTextItem>
#include <QObject>
#include <QMouseEvent>


class QCustomTextBox : public QGraphicsTextItem
{
	Q_OBJECT


public:
	QCustomTextBox(QString inp);
	QCustomTextBox();
protected:
	void mousePressEvent(QGraphicsSceneMouseEvent *event)override;

signals:
	void pressed();
};

#endif // QCUSTOMTEXTBOX_H
