#include "gamewidget.h"
#include "levels/level_0.h"
#include "levels/level_1.h"
#include "levels/level_2.h"
#include "levels/level_3.h"
#include "levels/level_4.h"
#include "levels/level_5.h"
#include "levels/level_6.h"
#include "levels/level_7.h"

void gameWidget::resizeEvent(QResizeEvent *event)
{
	refresh();
}

void gameWidget::refresh()
{
	volatile int wdh=width()-3;
	volatile int hdi=height()-3;// minus three because: 1 for satring at 0, and two for the uppre and lower border
	scene()->setSceneRect(0,0,wdh, hdi);

	level->refresh(updateinfo(hdi,level->camerapos, wdh*VHEIGHT_UNITS_F/ hdi));

}

gameWidget::gameWidget(levelids id)
{
	grabKeyboard();

	setMinimumSize(MIN_WINDOW_WIDTH, MIN_WINDOW_HEIGHT);
	setSizePolicy(QSizePolicy(QSizePolicy::MinimumExpanding,QSizePolicy::MinimumExpanding));
	loadLevel(id);
}

void gameWidget::loadLevel(levelids id){
	if(level!=NULL){
		delete level;
		level=NULL;
	}
	switch (id){
		case level0:
			level=new level_0();
			connect(level, SIGNAL(next(levelids)), this, SLOT(ll(levelids)));
			setScene(level);
			refresh();
		break;
		case level1:
			level=new level_1();
			connect(level, SIGNAL(next(levelids)), this, SLOT(ll(levelids)));
			setScene(level);
			refresh();
		break;
		case level2:
			level=new level_2();
			connect(level, SIGNAL(next(levelids)), this, SLOT(ll(levelids)));
			setScene(level);
			refresh();
		break;
		case level3:
			level=new level_3();
			connect(level, SIGNAL(next(levelids)), this, SLOT(ll(levelids)));
			setScene(level);
			refresh();
		break;
		case level4:
			level=new level_4();
			connect(level, SIGNAL(next(levelids)), this, SLOT(ll(levelids)));
			setScene(level);
			refresh();
		break;
		case level5:
			level=new level_5();
			connect(level, SIGNAL(next(levelids)), this, SLOT(ll(levelids)));
			setScene(level);
			refresh();
		break;
		case level6:
			level=new level_6();
			connect(level, SIGNAL(next(levelids)), this, SLOT(ll(levelids)));
			setScene(level);
			refresh();
		break;
		case level7:
			level=new level_7();
			connect(level, SIGNAL(credits()), this, SLOT(leaveGame()));
			setScene(level);
			refresh();
		break;



		case invalid:
		default:
			throw -1;

	}
}

void gameWidget::leaveGame()
{
	emit credits();
}

void gameWidget::ll(levelids id)
{
	loadLevel(id);
}
