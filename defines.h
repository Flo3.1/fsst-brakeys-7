#ifndef DEFINES_H
#define DEFINES_H




#define VHEIGHT_UNITS (512)
#define VHEIGHT_UNITS_F (VHEIGHT_UNITS*1.0)
#define BLOCKHEIGHT (32)

#define MIN_WINDOW_HEIGHT (VHEIGHT_UNITS)
#define MIN_WINDOW_WIDTH (BLOCKHEIGHT*8)

#define BLOCK_WALK_FACTOR (0)		/* wich wercentage of the height of the block is clipable
										some amount is needet for the physics to work */

#define PHYSICS_CLOCK (1000.0/60)


#define MAX_PLAYER_V_SPEED (12)
#define MAX_PLAYER_H_SPEED (8)
#define GRAVITY_STRENGHT (0.5)

#define FLAME_SPEED (50)

#define ANIMATION_SPEED (2) /* how fas the player animation is going to be played */

#define FRICTION (.4)	/* always akting against the direction of Movement */
#define GROUN_ACELLERATE (0.5+FRICTION) /* Real acceleration is 2 because we have to subtract friction*/
#define AIR_ACELLERATE (0.2)
#define JUMP_FORCE (16)

#endif // DEFINES_H
