#include "soupentity.h"

soupEntity::soupEntity(QGraphicsScene *scene, QVector2D pos, bool istop)
{
	objType="Soup";
	if(istop){
		setPixmap(QPixmap(":/recources/entities/soupEntity_top.png"));
		if(pixmap().isNull()){
			fprintf(stderr,"empty pixmap\n");

		}
	}else{
		setPixmap(QPixmap(":/recources/entities/soupEntity_normal.png"));
		if(pixmap().isNull()){
			fprintf(stderr,"empty pixmap\n");
		}
	}
	rsize={32,32};
	rpos=pos;
	scene->addItem(this);
}

