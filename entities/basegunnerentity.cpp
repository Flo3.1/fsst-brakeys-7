#include "basegunnerentity.h"

baseGunnerEntity::baseGunnerEntity()
{
	return;
}



void baseGunnerEntity::gametickint(updateinfo info, QList<Qt::Key> keys)
{
	long unsigned var=gen.generate64();
	if(var<= chance&& enableSpawn){
		//spawn bullet
		float angle=(sprayAngleOfset-sprayAngle/2+gen.generateDouble()*sprayAngle)*M_PI/180;
		bullets.push_back(new bulletEntity(scene(),rcenter()+muzzle,{-dir*cosf(angle)*speed, sinf(angle)*speed}));
		damager->push_back(bullets.last());

	}
	for(int i=0; i<bullets.size(); i++){
		if(bullets[i]->deleteMe){
			damager->removeAt(damager->indexOf(bullets[i]));
			delete bullets[i];
			bullets.removeAt(i);
		}
	}
	for(int i=0; i<bullets.length(); i++)
		bullets[i]->gametick(info, keys);
}

void baseGunnerEntity::repositionint(updateinfo info)
{
	if(rcenter().x()-rsize.x()/2<info.camerapos+info.widhtunits*.5 && rcenter().x()+rsize.x()/2>info.camerapos-info.widhtunits*.5){
		enableSpawn=true;
	}else{
		enableSpawn=false;
	}
	for(int i=0; i<bullets.length(); i++)
		bullets[i]->reposition(info);
}

void baseGunnerEntity::deletebullets()
{

	while (bullets.size()>0) {
		damager->removeAt(damager->indexOf(bullets[0]));
		delete bullets[0];
		bullets.pop_front();

	}
}
