#include "smithentity.h"

smithEntity::smithEntity(QGraphicsScene *scene, QList<baseEntity*>* dmgr, QVector2D pos, int direction, float angl, float ofs)
{
	sprayAngle=angl;
	sprayAngleOfset=ofs;
	dir=direction;
	muzzle={(float)(-13.0*dir),-19.0};
	chance=0x7fffffffffffffff;// a 50% chance to spawn a bullet
	//chance=0xffffffffffffffff;// a 100% chance to spawn a bullet
	rsize={32,64};
	setPixmap(QPixmap(":/recources/entities/smithEntity.png").transformed(QTransform(direction,0,0,1,0,0)));
	if(pixmap().isNull()){
		fprintf(stderr,"empty pixmao\n");

	}
	speed=6;
	rpos=pos;
	damager=dmgr;
	scene->addItem(this);
}

smithEntity::~smithEntity()
{
	deletebullets();
}


