#ifndef BULLETENTITY_H
#define BULLETENTITY_H

#include "baseentity.h"


class bulletEntity : public baseEntity
{
public:
	QVector2D speed;
	bool deleteMe=false;
	QVector2D start;
	bulletEntity(QGraphicsScene *scene, QVector2D pos={0,0}, QVector2D spd={0,0});
protected:
	void gametickint(updateinfo info, QList<Qt::Key> keys);
};

#endif // BULLETENTITY_H
