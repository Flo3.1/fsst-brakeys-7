#include "bulletentity.h"

bulletEntity::bulletEntity(QGraphicsScene *scene, QVector2D pos, QVector2D spd)
{
	objType="Bullet";
	setPixmap(QPixmap(":/recources/entities/bulletEntity.png").transformed(QTransform(spd.x()<0?1:-1,0,0,1,0,0)));
	if(pixmap().isNull()){
		fprintf(stderr,"empty pixmao\n");

	}
	rsize={4,4};
	rpos=pos-QVector2D(rsize.x()/2,0);
	speed=spd;
	start=rpos;
	scene->addItem(this);
}

void bulletEntity::gametickint(updateinfo info, QList<Qt::Key> keys)
{
	rpos+=speed;
	QList<QGraphicsItem*>test=collidingItems();
	if(test.size()>0 || (start-rpos).length()>1000)
		deleteMe=true;
	reposition(info);
}
