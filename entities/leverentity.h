#ifndef LEVERENTITY_H
#define LEVERENTITY_H

#include "baseentity.h"

class leverEntity : public baseEntity
{
public:
	bool *ispressed;
	leverEntity(QGraphicsScene *scene, bool*press, QVector2D pos={0,0});
protected:
	QPixmap image;
	bool pressedint=false;
	bool onscreen=true;
	void mousePressEvent(QGraphicsSceneMouseEvent *event)override;

	void repositionint(updateinfo info);
};

#endif // LEVERENTITY_H
