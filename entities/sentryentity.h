#ifndef SENTRYENTITY_H
#define SENTRYENTITY_H

#include "basegunnerentity.h"

class sentryEntity : public baseGunnerEntity
{
public:
	sentryEntity(QGraphicsScene *scene, QList<baseEntity*>* dmgr, QVector2D pos={0,0},  int direction=1, float angl=10, float ofs=0);
	~sentryEntity();
};

#endif // SENTRYENTITY_H
