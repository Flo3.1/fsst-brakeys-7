#include "fireentity.h"

fireEntity::fireEntity(QGraphicsScene *scene, QVector2D pos, int direction)
{
	dir=direction;
	objType="Fire";
	image[0]=QPixmap(":/recources/entities/fireEntity_long.png");
	for(int i=1;i<FLAME_SPEED; i++)
		image [i]=image[0].transformed(QTransform((FLAME_SPEED-i)*1.0/FLAME_SPEED,0,0,(FLAME_SPEED-i)*1.0/FLAME_SPEED,0,0));
	setPixmap(image[0].transformed(QTransform(dir,0,0,1,0,0)));
	if(pixmap().isNull()){
		fprintf(stderr,"empty pixmao\n");

	}
	rsize={128,32};
	rpos=pos;
	unpos=pos;
	scene->addItem(this);
	shown=true;
	stage=0;
}

void fireEntity::go()
{
	shown=false;
}

void fireEntity::come()
{
	shown=true;
}

void fireEntity::gametickint(updateinfo info, QList<Qt::Key> keys){
	if(shown){
		if(stage<FLAME_SPEED)
			stage++;
	}else{
		if(stage>0)
			stage--;
	}
	if(stage!=0){
		rpos=unpos;
		if(dir==1){
			rpos+={rsize.x()*(FLAME_SPEED-stage)/FLAME_SPEED,0};
		}

		rpos+={0,rsize.y()*(FLAME_SPEED-stage)/FLAME_SPEED/2};
		setPixmap(image[FLAME_SPEED-stage].transformed(QTransform(dir,0,0,1,0,0)));
	}else{

		rpos={0,VHEIGHT_UNITS_F*2};
	}
}
