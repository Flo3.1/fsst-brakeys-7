#ifndef SOUPENTITY_H
#define SOUPENTITY_H

#include "baseentity.h"

class soupEntity : public baseEntity
{
public:
	soupEntity(QGraphicsScene *scene, QVector2D pos={0,0}, bool istop=false);
};

#endif // SOUPENTITY_H
