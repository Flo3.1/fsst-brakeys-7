#include "playerentity.h"

playerEntity::playerEntity(QGraphicsScene *scene, QVector2D pos)
{


	run.push_back(QPixmap(":/recources/entities/playerEntity_idle.png"));
	run.push_back(QPixmap(":/recources/entities/playerEntity_running_1.png"));
	run.push_back(QPixmap(":/recources/entities/playerEntity_running_2.png"));
	run.push_back(QPixmap(":/recources/entities/playerEntity_running_3.png"));
	run.push_back(QPixmap(":/recources/entities/playerEntity_running_4.png"));
	run.push_back(QPixmap(":/recources/entities/playerEntity_jump.png"));



	objType="Player";
	setPixmap(run[standing]);
	if(pixmap().isNull()){
		fprintf(stderr,"empty pixmap\n");

	}
	state=standing;
	rsize={32,64};
	rpos=pos;
	speed={0,0};
	scene->addItem(this);
}


void playerEntity::gametickint(updateinfo info, QList<Qt::Key> keys)
{
	rpos+=speed;
	if(!onground){
		speed+={0,GRAVITY_STRENGHT};
		if(keys.indexOf(Qt::Key_A)!=-1){
			speed-={AIR_ACELLERATE,0};
		}
		if(keys.indexOf(Qt::Key_D)!=-1){
			speed+={AIR_ACELLERATE,0};
		}
	}else{
		if(speed.x()>FRICTION){
			speed-={FRICTION,0};
		}else if(speed.x()>0){
			speed.setX(0);
		}else if(speed.x()< -FRICTION){
			speed+={FRICTION,0};
		}else{
			speed.setX(0);
		}
		if(keys.indexOf(Qt::Key_A)!=-1){
			speed-={GROUN_ACELLERATE,0};
		}
		if(keys.indexOf(Qt::Key_D)!=-1){
			speed+={GROUN_ACELLERATE,0};
		}
		if(keys.indexOf(Qt::Key_Space)!=-1){
			speed+={0,-JUMP_FORCE};
			rpos+={0,-10};
		}
	}

	if(walls[left] && speed.x()<0){
		speed.setX(0);
	}
	if(walls[right] && speed.x()>0){
		speed.setX(0);
	}

	speed.setY(speed.y() >  MAX_PLAYER_V_SPEED ?  MAX_PLAYER_V_SPEED:speed.y());
	speed.setY(speed.y() < -MAX_PLAYER_V_SPEED ? -MAX_PLAYER_V_SPEED:speed.y());

	speed.setX(speed.x() >  MAX_PLAYER_H_SPEED ?  MAX_PLAYER_H_SPEED:speed.x());
	speed.setX(speed.x() < -MAX_PLAYER_H_SPEED ? -MAX_PLAYER_H_SPEED:speed.x());
	//printf("posy %f\n", rpos.y());



	//textures


	int dir=speed.x()>=0?1:-1;
	//if(onground){
		if(framecounter>=ANIMATION_SPEED*(12-abs(speed.x())) || state==standing){
			framecounter=0;
			switch(state){
				case standing:
				case run_4:
					state=run_1;
				break;

				case run_1:
				state=run_2;
				break;

				case run_2:
				state=run_3;
				break;

				case run_3:
				state=run_4;
				break;

			}


		}
		if(speed.x()==0){
			framecounter=0;
			//setPixmap(run[standing]);
			state=standing;
		}
	//}else{
	//	state=jumping;
		//state=standing; // the psysics doesnt like that
//	}
	setPixmap(run[state].transformed(QTransform(dir,0,0,1,0,0)));

	framecounter++;

}

void playerEntity::repositionint(updateinfo info)
{

	int dir=speed.x()>=0?1:-1;
	setPixmap(run[state].transformed(QTransform(dir,0,0,1,0,0)));
}
