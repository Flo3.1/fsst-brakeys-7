#ifndef FLAMEENTITY_H
#define FLAMEENTITY_H

#include "baseentity.h"
#include "fireentity.h"

class flameEntity : public baseEntity
{
public:
	flameEntity(QGraphicsScene *scene, QList<baseEntity*>* damager, QVector2D pos={0,0}, bool right=true);
	~flameEntity();
	fireEntity* fire;
	bool facingRight=true;
	void repositionint(updateinfo info);
	void gametickint(updateinfo info, QList<Qt::Key> keys);
};

#endif // FLAMEENTITY_H
