#include "flameentity.h"

flameEntity::flameEntity(QGraphicsScene *scene, QList<baseEntity *>* damager, QVector2D pos, bool right)
{
	objType="Flame";
	int direction=right?1:-1;

	setPixmap(QPixmap(":/recources/entities/flameEntity.png").transformed(QTransform(direction,0,0,1,0,0)));
	if(pixmap().isNull()){
		fprintf(stderr,"empty pixmao\n");

	}
	rsize={32,32};
	rpos=pos;
	fire=new fireEntity(scene, pos+QVector2D(right?-128:32,-3), direction);
	damager->push_back(fire);
	scene->addItem(this);
}

flameEntity::~flameEntity()
{
	delete fire;
}


void flameEntity::repositionint(updateinfo info){
	fire->reposition(info);
	if(rcenter().x()-rsize.x()/2<info.camerapos+info.widhtunits*.5 && rcenter().x()+rsize.x()/2>info.camerapos-info.widhtunits*.5){
		fire->come();
	}else{
		fire->go();
	}
}

void flameEntity::gametickint(updateinfo info, QList<Qt::Key> keys)
{
	fire->gametick(info, keys);
}
