#include "textureobject.h"



textureObject::textureObject(QGraphicsScene *scene, QString name, QVector2D pos)
{
	objType="Block";
	setPixmap(QPixmap(QString(":/recources/textures/")+name));
	if(pixmap().isNull()){
		fprintf(stderr,"empty pixmap %s\n", (QString(":/recources/textures/")+name).toStdString().c_str());

	}
	rsize=QVector2D(pixmap().size().width(), pixmap().size().height());
	rpos=pos;
	scene->addItem(this);
}
