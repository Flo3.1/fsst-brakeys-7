#ifndef FIREENTITY_H
#define FIREENTITY_H

#include "baseentity.h"

class fireEntity : public baseEntity
{
public:
	fireEntity(QGraphicsScene *scene, QVector2D pos={0,0}, int direction=1);
	QVector2D unpos;
	int stage;
	int dir;
	QPixmap image[FLAME_SPEED];
	bool shown;
	void go();
	void come();
protected:
	void gametickint(updateinfo info, QList<Qt::Key> keys);
};

#endif // FIREENTITY_H
