#include "textentity.h"

void textEntity::reposition(updateinfo info)
{

	setFont(QFont("Liberation Serif", (int)(info.heightpx/VHEIGHT_UNITS_F*16), 99, false));
	//setScale(info.heightpx/VHEIGHT_UNITS_F);
	setPos(((info.widhtunits/2+rpos.x()-info.camerapos)*info.heightpx)/VHEIGHT_UNITS_F-textWidth()/2, (rpos.y()*info.heightpx)/VHEIGHT_UNITS_F-(info.heightpx/VHEIGHT_UNITS_F*16)/2);
}

void textEntity::gametick(updateinfo info, QList<Qt::Key> keys)
{
}

textEntity::textEntity(QGraphicsScene *scene, QString name, QVector2D pos)
{
	rpos=pos;
	scene->addItem(this);
	setPlainText(name);
	setDefaultTextColor(Qt::white);
}


double textEntity::rcenterx()
{
	return rpos.x()+rsize.x()*.5;
}

double textEntity::rcentery()
{
	return rpos.y()+rsize.y()*.5;
}

QVector2D textEntity::rcenter()
{
	return rpos+rsize/2;
}
