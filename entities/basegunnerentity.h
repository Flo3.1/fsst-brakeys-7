#ifndef BASEGUNNERENTITY_H
#define BASEGUNNERENTITY_H

#include "baseentity.h"
#include "bulletentity.h"
#include <QRandomGenerator64>

class baseGunnerEntity : public baseEntity
{
public:

	bool enableSpawn;
	baseGunnerEntity();
	void deletebullets();
protected:
	QList<bulletEntity*> bullets;
	QVector2D muzzle;
	float sprayAngle=0;
	float sprayAngleOfset=0;
	float speed;
	QList<baseEntity*>* damager;
	int dir;
	long unsigned chance; // in a 64 bit number
	// so a chanze of 0 would be one in 2^64 and a chance of (2^65)-1 would mean half the time
	QRandomGenerator64 gen;
	void gametickint(updateinfo info, QList<Qt::Key> keys);
	void repositionint(updateinfo info);
};

#endif // BASEGUNNERENTITY_H
