#include "leverentity.h"


leverEntity::leverEntity(QGraphicsScene *scene, bool*press, QVector2D pos)
{
	ispressed=press;
	objType="Lever";
	image=QPixmap(":/recources/entities/leverEntity_1.png");
	setPixmap(image);
	if(pixmap().isNull()){
		fprintf(stderr,"empty pixmao\n");

	}
	rsize={32,32};
	rpos=pos;
	scene->addItem(this);
}

void leverEntity::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
	pressedint= !pressedint;
	*ispressed=pressedint&&onscreen;

	setPixmap(image.transformed(QTransform(1,0,0,pressedint?-1:1,0,0)));
}

void leverEntity::repositionint(updateinfo info)
{
	if(rcenter().x()-rsize.x()/2<info.camerapos+info.widhtunits*.5 && rcenter().x()+rsize.x()/2>info.camerapos-info.widhtunits*.5){
		onscreen=true;
	}else{
		onscreen=false;
	}

	*ispressed=pressedint&&onscreen;
}
