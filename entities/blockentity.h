#ifndef BLOCKENTITY_H
#define BLOCKENTITY_H

#include "baseentity.h"

class blockEntity: public baseEntity
{
public:
	blockEntity(QGraphicsScene *scene, QVector2D pos={0,0}, bool portal=false);
};

#endif // BLOCKENTITY_H
