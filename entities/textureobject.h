#ifndef TEXTUREOBJECT_H
#define TEXTUREOBJECT_H

#include "baseentity.h"

class textureObject : public baseEntity
{
public:
	textureObject(QGraphicsScene *scene, QString name, QVector2D pos={0,0});
};

#endif // TEXTUREOBJECT_H
