#ifndef BASEENTITY_H
#define BASEENTITY_H

#include <QGraphicsPixmapItem>
#include <QVector2D>
#include <QGraphicsScene>
#include "../defines.h"
#include "../updateinfo.h"

class baseEntity : public QGraphicsPixmapItem
{
	// Q_OBJECT	// You can not use the Q_OBJECT Makro witout the object inheriting from the QObject class
	// All entities are inable of using signals & slots
	// you culd use multi inheritance withe the QObjet if you want

public:
	QVector2D rpos;
	QVector2D rsize;
	//only usefull for objects osed once per level, so there are no casting errors
	QString objType;
	// the height of the screen in pixel
	// the camera start in units
	// the camera end  in units
	void reposition(updateinfo info);
	void gametick(updateinfo info, QList<Qt::Key> keys);
	QString name;
	baseEntity();

	double rcenterx();
	double rcentery();
	QVector2D rcenter();

protected:
	//probably useless, but for completnes sake
	virtual void repositionint(updateinfo info){};
	//thsi is way more important than the reposition thingy
	virtual void gametickint(updateinfo info, QList<Qt::Key> keys){};
};

#endif // BASEENTITY_H
