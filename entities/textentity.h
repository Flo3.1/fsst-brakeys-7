#ifndef TEXTENTITY_H
#define TEXTENTITY_H

#include <QVector2D>
#include <QFont>
#include <QGraphicsTextItem>
#include <QGraphicsScene>
#include "../updateinfo.h"
#include "defines.h"

class textEntity : public QGraphicsTextItem
{
public:
	QVector2D rpos;
	QVector2D rsize;
	//only usefull for objects osed once per level, so there are no casting errors
	QString objType;
	// the height of the screen in pixel
	// the camera start in units
	// the camera end  in units
	void reposition(updateinfo info);
	void gametick(updateinfo info, QList<Qt::Key> keys);
	QString name;
	textEntity(QGraphicsScene *scene, QString name, QVector2D pos={0,0});

	double rcenterx();
	double rcentery();
	QVector2D rcenter();
};

#endif // TEXTENTITY_H
