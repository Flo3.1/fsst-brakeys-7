#ifndef PLAYERENTITY_H
#define PLAYERENTITY_H

#include "baseentity.h"

enum wallpos{right,left};
enum runstate{standing, run_1, run_2, run_3, run_4, jumping};

class playerEntity : public baseEntity
{
public:
	QVector2D speed;
	runstate state;
	playerEntity(QGraphicsScene *scene, QVector2D pos={0,0});
	QList<QPixmap> run;
	int framecounter=0;
	bool onground=false;
	bool walls[2]={false,false};
protected:
	void gametickint(updateinfo info, QList<Qt::Key> keys);
	void repositionint(updateinfo info);
};

#endif // PLAYERENTITY_H
