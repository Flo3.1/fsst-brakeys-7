#include "baseentity.h"

void baseEntity::reposition(updateinfo info)
{
	setPos(((info.widhtunits/2+rpos.x()-info.camerapos)*info.heightpx)/VHEIGHT_UNITS_F, (rpos.y()*info.heightpx)/VHEIGHT_UNITS_F);
	setScale(info.heightpx/VHEIGHT_UNITS_F);
	repositionint(info);
}

void baseEntity::gametick(updateinfo info, QList<Qt::Key> keys)
{
	gametickint(info, keys);
}

baseEntity::baseEntity()
{

}

double baseEntity::rcenterx()
{
	return rpos.x()+rsize.x()*.5;
}

double baseEntity::rcentery()
{
	return rpos.y()+rsize.y()*.5;
}

QVector2D baseEntity::rcenter()
{
	return rpos+rsize/2;
}
