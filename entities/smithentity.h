#ifndef SMITHENTITY_H
#define SMITHENTITY_H

#include "basegunnerentity.h"

class smithEntity : public baseGunnerEntity
{
public:
	smithEntity(QGraphicsScene *scene, QList<baseEntity*>* dmgr, QVector2D pos={0,0},  int direction=1, float angl=10, float ofs=0);
	~smithEntity();
};

#endif // SMITHENTITY_H
