#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "menuwidget.h"
#include "gamewidget.h"
#include "levels/baselevel.h"

/*
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE
*/

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow(QWidget *parent = nullptr);
	~MainWindow();
protected:

	void resizeEvent(QResizeEvent *event)override;

private:
	class menuWidget *menu=NULL;
	class gameWidget *game=NULL;
	//Ui::MainWindow *ui;
public slots:
	void startLevel(levelids id);
	void mainMenu();
	void showCredits();


};
#endif // MAINWINDOW_H
