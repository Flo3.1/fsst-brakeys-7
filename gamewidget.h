#ifndef GAMEWIDGET_H
#define GAMEWIDGET_H

#include <QGraphicsView>
#include <QObject>
#include "levels/baselevel.h"

class gameWidget : public QGraphicsView
{
	Q_OBJECT

protected:
	void resizeEvent(QResizeEvent *event)override;
public:
	void refresh();
	gameWidget(levelids id);
	baseLevel* level=NULL;
	void loadLevel(levelids id);

signals:
	void credits();
public slots:
	void leaveGame();
	void ll(levelids id);

};

#endif // GAMEWIDGET_H
