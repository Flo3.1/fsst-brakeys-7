#ifndef MENUWIDGET_H
#define MENUWIDGET_H

#include <QGraphicsView>
#include <QObject>
#include "qcustomtextbox.h"
#include "menus/mainmenuscene.h"
#include "menus/creditscene.h"
#include "menus/levelmenu.h"
#include "levels/baselevel.h"

class menuWidget : public QGraphicsView
{
	Q_OBJECT
public:
	menuWidget();
	menuWidget(bool credits);
	~menuWidget();

protected:
	void resizeEvent(QResizeEvent *event)override;
private:
	void intit();
	levelMenu * levelMen;
	mainMenuScene * mainMenu;
	creditScene * cretidMenu;

	void reposition();
signals:
	void startLevel(levelids id);
public slots:
	void playlv1();
	void playlv2();
	void playlv3();
	void playlv4();
	void playlv5();
	void playlv6();
	void exit();
	void credits();
	void menu();
	void levels();
};

#endif // MENUWIDGET_H
