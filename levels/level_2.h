#ifndef LEVEL_2_H
#define LEVEL_2_H

#include "baselevel.h"

class level_2 : public baseLevel
{
public:
	explicit level_2(QObject *parent = nullptr);
};

#endif // LEVEL_2_H
