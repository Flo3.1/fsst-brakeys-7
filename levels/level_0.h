#ifndef LEVEL_0_H
#define LEVEL_0_H

#include "baselevel.h"
#include "../entities/leverentity.h"
#include <QObject>

class level_0 : public baseLevel
{
	Q_OBJECT
public:
	bool leverbol;
	leverEntity *lever;
	explicit level_0(QObject *parent = nullptr);
	~level_0();
	void refreshint(updateinfo info);
};

#endif // LEVEL_0_H
