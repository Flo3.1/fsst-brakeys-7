#include "baselevel.h"

baseLevel::baseLevel(QObject *parent, bool isportal) : QGraphicsScene(parent)
{

	setBackgroundBrush(QBrush(QColor(0x0d,0x21,0x00)));
	ticks.setInterval(PHYSICS_CLOCK);
	connect(&ticks, SIGNAL(timeout()), this, SLOT(timerIsr()));
	ticks.start();


	if(isportal){
		spawn=new textureObject(this, "portalEntity_1.png", {0,0});
	}else{
		spawn=new textureObject(this, "matrixEntity.png", {0,0});
	}

	if(isportal){
		goal=new textureObject(this, "portalEntity_2.png", {0,0});
	}else{
		goal=new textureObject(this, "matrixEntity.png", {0,0});
	}

	textures.push_back(goal);
	textures.push_back(spawn);

	player=new playerEntity(this, {0, 0});
	//addItem(player);

}

baseLevel::~baseLevel()
{
	delete player;
	while (blocks.size()>0) {
		delete blocks[0];
		blocks.pop_front();
	}
	while (soups.size()>0) {
		delete soups[0];
		soups.pop_front();
	}
	while (flames.size()>0) {
		delete flames[0];
		flames.pop_front();
	}
	while (textures.size()>0) {
		delete textures[0];
		textures.pop_front();
	}
	while (gunner.size()>0) {
		delete gunner[0];
		gunner.pop_front();
	}
	while (texts.size()>0) {
		delete texts[0];
		texts.pop_front();
	}
}

void baseLevel::refresh(updateinfo info)
{

	intnfo=info;
	info.camerapos=player->rpos.x()+16;

	for(int i=0; i<blocks.length(); i++)
		blocks[i]->reposition(info);

	for(int i=0; i<soups.length(); i++)
		soups[i]->reposition(info);

	for(int i=0; i<flames.length(); i++)
		flames[i]->reposition(info);

	for(int i=0; i<gunner.length(); i++)
		gunner[i]->reposition(info);

	for(int i=0; i<textures.length(); i++)
		textures[i]->reposition(info);

	for(int i=0; i<texts.length(); i++)
		texts[i]->reposition(info);

	player->reposition(info);

	refreshint(info);



}

void baseLevel::gametick(updateinfo info)
{
	//printf("segfault1?\n");
	if(player->collidesWithItem(goal)){
		emit next(nextlevel);
		return;
	}

//	refresh(info);
	player->onground=false;
	player->walls[right]=false;
	player->walls[left]=false;
	for(int i=0; i<colliders.length(); i++){
		if(player->collidesWithItem(colliders[i])){

			if(	colliders[i]->rcenter().x()-colliders[i]->rsize.x()/2+1 < player->rcenter().x()+player->rsize.x()/2
					&&
				colliders[i]->rcenter().x()+colliders[i]->rsize.x()/2-1 > player->rcenter().x()-player->rsize.x()/2
				){


				if(!player->onground && colliders[i]->rcenter().y()-colliders[i]->rsize.y()*BLOCK_WALK_FACTOR > player->rcenter().y()+player->rsize.y()/2){
					player->onground=true;
					player->speed.setY(-GRAVITY_STRENGHT*1.1);
					player->rpos.setY(colliders[i]->rpos.y()-player->rsize.y()+1);
				}
				if(colliders[i]->rcenter().y()+colliders[i]->rsize.y()*0< player->rcenter().y()-player->rsize.y()/2){
					player->rpos.setY(colliders[i]->rpos.y()+colliders[i]->rsize.y()+2);
					player->speed.setY(0);
				}

			}
			if(colliders[i]->rcenter().y()-colliders[i]->rsize.y()/2+1 < player->rcenter().y()+player->rsize.y()/2){
				if(colliders[i]->rcenter().x()-colliders[i]->rsize.x()*BLOCK_WALK_FACTOR > player->rcenter().x()+player->rsize.x()/2){
					player->walls[right]=true;
					player->rpos.setX(colliders[i]->rpos.x()-player->rsize.x()+1);
				}
				if(colliders[i]->rcenter().x()+colliders[i]->rsize.x()*BLOCK_WALK_FACTOR < player->rcenter().x()-player->rsize.x()/2){
					player->walls[left]=true;
					player->rpos.setX(colliders[i]->rpos.x()+player->rsize.x()-1);
				}
			}

		}

	}

	if(player->rpos.y()>VHEIGHT_UNITS_F){
		reSpawn();
	}


	player->gametick(info, keys);

	for(int i=0; i<damager.length(); i++){
		if(player->collidesWithItem(damager[i])){
			reSpawn();
		}

	}



	for(int i=0; i<blocks.length(); i++)
		blocks[i]->gametick(info, keys);

	for(int i=0; i<flames.length(); i++)
		flames[i]->gametick(info, keys);

	for(int i=0; i<gunner.length(); i++)
		gunner[i]->gametick(info, keys);



	gametickint(info);
	//printf("segfault2?\n");
	refresh(info);
}

void baseLevel::reSpawn()
{
	for(int i=0; i<gunner.length(); i++)
		gunner[i]->deletebullets();
	player->rpos=spawn->rpos;
	player->speed={0,0};
}

void baseLevel::keyPressEvent(QKeyEvent *event)
{
	if(keys.indexOf((Qt::Key)event->key())==-1){
		keys.push_back((Qt::Key)event->key());	// Why is this cast neseceary?????
	}
}

void baseLevel::keyReleaseEvent(QKeyEvent *event)
{
	if(keys.indexOf((Qt::Key)event->key())!=-1){
		keys.removeAt(keys.indexOf((Qt::Key)event->key()));
	}
}

void baseLevel::timerIsr()
{
	gametick(intnfo);
}
