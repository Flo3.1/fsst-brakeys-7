#include "level_7.h"

level_7::level_7(QObject *parent)
	: baseLevel{parent, true}
{
	spawn->rpos={BLOCKHEIGHT*6, BLOCKHEIGHT*0};
	goal->rpos={BLOCKHEIGHT*90, BLOCKHEIGHT*13};

	for(int i=0; i<15; i++)
		blocks.push_back(new blockEntity(this, {(float)0,(float)i*BLOCKHEIGHT}, true));
	for(int i=0; i<13; i++)
		blocks.push_back(new blockEntity(this, {(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*15}, true));
	for(int i=0; i<15; i++)
		blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*12,(float)i*BLOCKHEIGHT}, true));
	for(int i=0; i<12; i++)
		flames.push_back(new flameEntity(this, &notDamager,{(float)BLOCKHEIGHT*1,(float)i*BLOCKHEIGHT}, false));
	for(int i=0; i<12; i++)
		flames.push_back(new flameEntity(this, &notDamager,{(float)BLOCKHEIGHT*11,(float)i*BLOCKHEIGHT}, true));
	for(int i=12; i<15; i++)
		flames.push_back(new flameEntity(this, &notDamager,{(float)BLOCKHEIGHT*2,(float)i*BLOCKHEIGHT}, false));
	for(int i=12; i<15; i++)
		flames.push_back(new flameEntity(this, &notDamager,{(float)BLOCKHEIGHT*10,(float)i*BLOCKHEIGHT}, true));
	for(int i=12; i<15; i++)
		blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*1,(float)i*BLOCKHEIGHT}, true));
	for(int i=12; i<15; i++)
		blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*11,(float)i*BLOCKHEIGHT}, true));

	reSpawn();
}

void level_7::gametickint(updateinfo info)
{
	for(int i=0; i<notDamager.length(); i++){
		if(player->collidesWithItem(notDamager[i])){
			emit credits();
		}

	}
}
