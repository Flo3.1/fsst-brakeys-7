#include "level_2.h"
#include "../entities/smithentity.h"

level_2::level_2(QObject *parent)
	: baseLevel{parent}
{
	spawn->rpos={BLOCKHEIGHT*2, BLOCKHEIGHT*13};
	goal->rpos={BLOCKHEIGHT*90, BLOCKHEIGHT*13};


	gunner.push_back(new smithEntity(this, &damager,{(float)BLOCKHEIGHT*1,(float)BLOCKHEIGHT*6}, -1, 25, 18));
	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*1,(float)BLOCKHEIGHT*8}));
	texts.push_back(new textEntity(this, "! This is a very dangerous place\n! They have installed some security software\n! As you can see, it is beateable\n!", {(float)BLOCKHEIGHT*9,(float)BLOCKHEIGHT*8}));
	texts.push_back(new textEntity(this, "Try to be careful, we depend on you", {(float)BLOCKHEIGHT*35,(float)BLOCKHEIGHT*8}));
	texts.push_back(new textEntity(this, "Think INSIDE the box", {(float)BLOCKHEIGHT*71,(float)BLOCKHEIGHT*3.5}));

	for(int i=0; i<15; i++)
		blocks.push_back(new blockEntity(this, {(float)0,(float)i*BLOCKHEIGHT}));
	for(int i=0; i<101; i++)
		blocks.push_back(new blockEntity(this, {(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*15}));
	for(int i=0; i<15; i++)
		blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*100,(float)i*BLOCKHEIGHT}));
	for(int i=7; i<15; i++)
		blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*81,(float)i*BLOCKHEIGHT}));

	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*65,(float)BLOCKHEIGHT*14}));
	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*65,(float)BLOCKHEIGHT*13}));
	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*73,(float)BLOCKHEIGHT*9}));
	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*72,(float)BLOCKHEIGHT*9}));
	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*67,(float)BLOCKHEIGHT*4}));
	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*66,(float)BLOCKHEIGHT*4}));
	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*72,(float)BLOCKHEIGHT*2}));
	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*73,(float)BLOCKHEIGHT*2}));
	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*74,(float)BLOCKHEIGHT*2}));
	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*75,(float)BLOCKHEIGHT*2}));

	flames.push_back(new flameEntity(this, &damager,{(float)BLOCKHEIGHT*31,(float)BLOCKHEIGHT*14}, false));
	flames.push_back(new flameEntity(this, &damager,{(float)BLOCKHEIGHT*30,(float)BLOCKHEIGHT*14}, true));

	gunner.push_back(new smithEntity(this, &damager,{(float)BLOCKHEIGHT*80,(float)BLOCKHEIGHT*13}, 1, 10, 0));
	gunner.push_back(new smithEntity(this, &damager,{(float)BLOCKHEIGHT*81,(float)BLOCKHEIGHT*5}, 1, 10, 0));

	for(int i=0; i<blocks.length(); i++){
		colliders.push_back(blocks[i]);
	}

	for(int i=0; i<gunner.length(); i++){
		damager.push_back(gunner[i]);
	}

	nextlevel=level3;
	reSpawn();
}
