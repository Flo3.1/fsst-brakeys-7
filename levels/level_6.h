#ifndef LEVEL_6_H
#define LEVEL_6_H
#include "../entities/leverentity.h"

#include "baselevel.h"

class level_6 : public baseLevel
{
protected:
	void refreshint(updateinfo info);
	leverEntity* lever;
	bool leverpressed;
	leverEntity* lever2;
	bool leverpressed2;
	leverEntity* lever3;
	bool leverpressed3;
public:
	explicit level_6(QObject *parent = nullptr);
	~level_6();
};

#endif // LEVEL_6_H
