#ifndef BASELEVEL_H
#define BASELEVEL_H

#include <QGraphicsScene>
#include <QObject>
#include <QTimer>
#include <QKeyEvent>
#include "../entities/textentity.h"
#include "../entities/blockentity.h"
#include "../entities/playerentity.h"
#include "../entities/soupentity.h"
#include "../entities/flameentity.h"
#include "../entities/basegunnerentity.h"
#include "../entities/textureobject.h"

enum levelids{level0, level1, level2, level3, level4, level5, level6, level7, invalid};


class baseLevel : public QGraphicsScene
{
	Q_OBJECT
public:
	int camerapos=0;
	explicit baseLevel(QObject *parent = nullptr, bool isportal=false);
	~baseLevel();
	void refresh(updateinfo info);
	void gametick(updateinfo info);



protected:
	//Vector2D spawn;

	textureObject *spawn;
	textureObject *goal;

	QList<textEntity*> texts;

	bool isportal=false;
	levelids nextlevel=level0;
	QList<baseEntity*> colliders;
	QList<baseEntity*> damager;

	QList<textureObject*> textures;

	QList<baseGunnerEntity*> gunner;
	QList<soupEntity*> soups;
	QList<blockEntity*> blocks;
	QList<flameEntity*> flames;
	playerEntity * player;
	QList<Qt::Key> keys;
	updateinfo intnfo;
	QTimer ticks;
	void reSpawn();
	void keyPressEvent(QKeyEvent *event)override;
	void keyReleaseEvent(QKeyEvent *event)override;

	//the split is to diferentiate between physics and level specific code
	virtual void refreshint(updateinfo info){};
	virtual void gametickint(updateinfo info){};
signals:
	void next(levelids id);


public slots:

	void timerIsr();

};

#endif // BASELEVEL_H
