#include "level_5.h"

level_5::level_5(QObject *parent)
	: baseLevel{parent}
{
	spawn->rpos={BLOCKHEIGHT*2, BLOCKHEIGHT*1};
	goal->rpos={BLOCKHEIGHT*92, BLOCKHEIGHT*1};

	texts.push_back(new textEntity(this, "Their final firewall\nIt's a bit tight", {(float)BLOCKHEIGHT*4,(float)BLOCKHEIGHT*1}));
	texts.push_back(new textEntity(this, "You have made it\nNow get out of here", {(float)BLOCKHEIGHT*94,(float)BLOCKHEIGHT*1}));

	for(int i=0; i<7; i++)
		blocks.push_back(new blockEntity(this, {(float)0,(float)i*BLOCKHEIGHT}));
	for(int i=8; i<15; i++)
		blocks.push_back(new blockEntity(this, {(float)0,(float)i*BLOCKHEIGHT}));
	for(int i=0; i<101; i++)
		blocks.push_back(new blockEntity(this, {(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*15}));
	for(int i=0; i<7; i++)
		blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*100,(float)i*BLOCKHEIGHT}));
	for(int i=8; i<15; i++)
		blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*100,(float)i*BLOCKHEIGHT}));

	for(int i=0; i<100; i=i+10){
		blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*1+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*3}));
		blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*2+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*3}));
		blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*3+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*3}));
		blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*4+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*3}));
		blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*5+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*3}));
		blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*5+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*7}));
		blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*6+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*7}));
		blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*7+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*7}));
		blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*8+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*7}));
		blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*9+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*7}));
		blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*1+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*11}));
		blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*2+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*11}));
		blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*3+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*11}));
		blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*4+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*11}));
		blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*5+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*11}));
		flames.push_back(new flameEntity(this, &damager,{(float)BLOCKHEIGHT*0+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*7}, false));
		flames.push_back(new flameEntity(this, &damager,{(float)BLOCKHEIGHT*10+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*3}, true));
		flames.push_back(new flameEntity(this, &damager,{(float)BLOCKHEIGHT*10+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*11}, true));
		blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*10+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*4}));
		blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*10+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*5}));
		blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*10+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*6}));
		blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*10+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*8}));
		blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*10+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*9}));
		blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*10+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*10}));
		if(i/10%2==0){
			blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*10+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*-6}));
			blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*10+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*-5}));
			blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*10+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*-4}));
			blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*10+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*-3}));
			blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*10+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*-2}));
			blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*10+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*-1}));
			blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*10+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*0}));
			blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*10+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*1}));
			blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*10+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*2}));
			blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*10+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*12}));
		}
		else{
			blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*10+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*12}));
			blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*10+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*13}));
			blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*10+(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*14}));
		}
	}


	for(int i=0; i<blocks.length(); i++){
		colliders.push_back(blocks[i]);
	}
	nextlevel=level6;
	reSpawn();
}
