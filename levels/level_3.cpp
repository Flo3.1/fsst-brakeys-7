#include "level_3.h"
#include "../entities/smithentity.h"

level_3::level_3(QObject *parent)
	: baseLevel{parent}
{
	spawn->rpos={BLOCKHEIGHT*2, BLOCKHEIGHT*13};
	goal->rpos={BLOCKHEIGHT*90, BLOCKHEIGHT*13};
	texts.push_back(new textEntity(this, "Be careful, this server is already infected with a virus", {(float)BLOCKHEIGHT*10,(float)BLOCKHEIGHT*8}));
	for(int i=0; i<15; i++)
		blocks.push_back(new blockEntity(this, {(float)0,(float)i*BLOCKHEIGHT}));
	for(int i=0; i<30; i++)
		blocks.push_back(new blockEntity(this, {(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*15}));
	for(int i=61; i<101; i++)
		blocks.push_back(new blockEntity(this, {(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*15}));
	for(int i=0; i<15; i++)
		blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*100,(float)i*BLOCKHEIGHT}));
	for(int i=30; i<=60; i++)
		soups.push_back(new soupEntity(this, {(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*15},true));
	for(int i=33; i<36; i++)
		blocks.push_back(new blockEntity(this, {(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*11}));
	for(int i=43; i<46; i++)
		blocks.push_back(new blockEntity(this, {(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*12}));
	for(int i=53; i<56; i++)
		blocks.push_back(new blockEntity(this, {(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*11}));

	flames.push_back(new flameEntity(this, &damager,{(float)BLOCKHEIGHT*76,(float)BLOCKHEIGHT*14}, false));
	flames.push_back(new flameEntity(this, &damager,{(float)BLOCKHEIGHT*75,(float)BLOCKHEIGHT*14}, true));

	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*24,(float)BLOCKHEIGHT*12}));
	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*62,(float)BLOCKHEIGHT*12}));

	gunner.push_back(new smithEntity(this, &damager,{(float)BLOCKHEIGHT*24,(float)BLOCKHEIGHT*10}, -1, 10, 0));
	gunner.push_back(new smithEntity(this, &damager,{(float)BLOCKHEIGHT*62,(float)BLOCKHEIGHT*10}, 1, 10, 0));


	for(int i=0; i<gunner.length(); i++){
		damager.push_back(gunner[i]);
	}

	for(int i=0; i<blocks.length(); i++){
		colliders.push_back(blocks[i]);
	}

	nextlevel=level4;
	reSpawn();
}
