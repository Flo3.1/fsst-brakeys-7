#include "level_1.h"

level_1::level_1(QObject *parent) : baseLevel(parent)
{
	nextlevel=level2;
	spawn->rpos={BLOCKHEIGHT*2, BLOCKHEIGHT*13};
	goal->rpos={BLOCKHEIGHT*123, BLOCKHEIGHT*13};

	texts.push_back(new textEntity(this, "Hello\nYou are finally reachable", {(float)BLOCKHEIGHT*5,(float)BLOCKHEIGHT*8}));
	texts.push_back(new textEntity(this, "We have tried to establish a connection for ages", {(float)BLOCKHEIGHT*15,(float)BLOCKHEIGHT*8}));
	texts.push_back(new textEntity(this, "You are IN the internet and ………\nDON'T JUMP IN THERE", {(float)BLOCKHEIGHT*35,(float)BLOCKHEIGHT*8}));
	texts.push_back(new textEntity(this, "…… as I was saying, you are in the internet wich is beeing attacked by INSTATUBE™\nThe thing you almost jumped into was a liquified computer virus", {(float)BLOCKHEIGHT*50,(float)BLOCKHEIGHT*8}));
	texts.push_back(new textEntity(this, "We need you to penetrate their firewall to gain acess to their systems\nTo train you we have built a custom training course here", {(float)BLOCKHEIGHT*80,(float)BLOCKHEIGHT*8}));
	texts.push_back(new textEntity(this, "Their defences can't hurt you once they are out of your periphery\nIt was designed to save computer recources\nBut you can use that to your advantage by looking more narrowly", {(float)BLOCKHEIGHT*95,(float)BLOCKHEIGHT*2}));


	texts.push_back(new textEntity(this, "This is a bitstream\nIt takes you to the next server", {(float)BLOCKHEIGHT*120,(float)BLOCKHEIGHT*8}));
	for(int i=0; i<15; i++)
		blocks.push_back(new blockEntity(this, {(float)0,(float)i*BLOCKHEIGHT}));

	for(int i=0; i<130; i++)
		blocks.push_back(new blockEntity(this, {(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*15}));

	for(int i=0; i<15; i++)
		blocks.push_back(new blockEntity(this, {(float)129*BLOCKHEIGHT,(float)i*BLOCKHEIGHT}));


	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*17,(float)BLOCKHEIGHT*14}));

	for(int i=13; i<15; i++)
		blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*24,(float)i*BLOCKHEIGHT}));


	for(int i=12; i<15; i++)
		blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*31,(float)i*BLOCKHEIGHT}));



	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*40,(float)BLOCKHEIGHT*14}));

	for(int i=41; i<48; i++)
		soups.push_back(new soupEntity(this, {(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*14}, true));


	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*48,(float)BLOCKHEIGHT*14}));



	blocks.push_back(new blockEntity(this, {(float)105*BLOCKHEIGHT,(float)10*BLOCKHEIGHT}));
	blocks.push_back(new blockEntity(this, {(float)104*BLOCKHEIGHT,(float)10*BLOCKHEIGHT}));
	blocks.push_back(new blockEntity(this, {(float)103*BLOCKHEIGHT,(float)10*BLOCKHEIGHT}));



	for(int i=7; i<15; i++)
		blocks.push_back(new blockEntity(this, {(float)110*BLOCKHEIGHT,(float)i*BLOCKHEIGHT}));
	for(int i=0; i<7; i++)
		blocks.push_back(new blockEntity(this, {(float)115*BLOCKHEIGHT,(float)i*BLOCKHEIGHT}));
	for(int i=8; i<13; i++)
		blocks.push_back(new blockEntity(this, {(float)115*BLOCKHEIGHT,(float)i*BLOCKHEIGHT}));


	blocks.push_back(new blockEntity(this, {(float)109*BLOCKHEIGHT,(float)7*BLOCKHEIGHT}));
	blocks.push_back(new blockEntity(this, {(float)108*BLOCKHEIGHT,(float)7*BLOCKHEIGHT}));

	flames.push_back(new flameEntity(this, &damager,{(float)BLOCKHEIGHT*115,(float)BLOCKHEIGHT*7}, true));
	blocks.push_back(new blockEntity(this, {(float)115*BLOCKHEIGHT,(float)12*BLOCKHEIGHT}));


	for(int i=0; i<blocks.length(); i++){
		colliders.push_back(blocks[i]);
	}
	for(int i=0; i<flames.length(); i++){

		colliders.push_back(flames[i]);
	}
	for(int i=0; i<soups.length(); i++){

		damager.push_back(soups[i]);
	}

	nextlevel=level2;
	reSpawn();
}
