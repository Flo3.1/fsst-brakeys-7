#include "level_0.h"
#include "../entities/smithentity.h"
#include "../entities/sentryentity.h"

level_0::level_0(QObject *parent) : baseLevel(parent, true)
{
	spawn->rpos={BLOCKHEIGHT*2, BLOCKHEIGHT*13};
	goal->rpos={BLOCKHEIGHT*60, BLOCKHEIGHT*13};

	texts.push_back(new textEntity(this, "Hello\nYou are finally reachable", {(float)BLOCKHEIGHT*5,(float)BLOCKHEIGHT*8}));
	for(int i=0; i<16; i++)
		blocks.push_back(new blockEntity(this, {(float)0,(float)i*BLOCKHEIGHT}));
	for(int i=0; i<101; i++)
		blocks.push_back(new blockEntity(this, {(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*15}));


	for(int i=1; i<15; i++)
		soups.push_back(new soupEntity(this, {(float)i*BLOCKHEIGHT+(float)BLOCKHEIGHT*4,(float)BLOCKHEIGHT*14},false));
	for(int i=1; i<15; i++)
		soups.push_back(new soupEntity(this, {(float)i*BLOCKHEIGHT+(float)BLOCKHEIGHT*4,(float)BLOCKHEIGHT*13},false));
	for(int i=1; i<15; i++)
		soups.push_back(new soupEntity(this, {(float)i*BLOCKHEIGHT+(float)BLOCKHEIGHT*4,(float)BLOCKHEIGHT*12},true));

	flames.push_back(new flameEntity(this, &damager,{(float)BLOCKHEIGHT*31,(float)BLOCKHEIGHT*14}, false));
	flames.push_back(new flameEntity(this, &damager,{(float)BLOCKHEIGHT*30,(float)BLOCKHEIGHT*14}, true));


	gunner.push_back(new smithEntity(this, &damager,{(float)BLOCKHEIGHT*80,(float)BLOCKHEIGHT*13}, 1, 10, 0));
	gunner.push_back(new smithEntity(this, &damager,{(float)BLOCKHEIGHT*70,(float)BLOCKHEIGHT*13}, -1, 10, 0));

	gunner.push_back(new sentryEntity(this, &damager,{(float)BLOCKHEIGHT*69,(float)BLOCKHEIGHT*14}, 1, 5, -20));


	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*4,(float)BLOCKHEIGHT*14}));
	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*4,(float)BLOCKHEIGHT*13}));
	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*4,(float)BLOCKHEIGHT*12}));
	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*4,(float)BLOCKHEIGHT*11}));
	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*4,(float)BLOCKHEIGHT*10}));
	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*5,(float)BLOCKHEIGHT*10}));
	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*5,(float)BLOCKHEIGHT*10}));


	for(int i=0; i<blocks.length(); i++){
		colliders.push_back(blocks[i]);
	}
	for(int i=0; i<flames.length(); i++){

		colliders.push_back(flames[i]);
	}
	for(int i=0; i<soups.length(); i++){

		damager.push_back(soups[i]);
	}
	lever=new leverEntity(this, &leverbol,{BLOCKHEIGHT*2, 0});
	reSpawn();
}

level_0::~level_0()
{
	delete lever;
}

void level_0::refreshint(updateinfo info)
{
	lever->reposition(info);
}
