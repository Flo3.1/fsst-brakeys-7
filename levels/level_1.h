#ifndef LEVEL_1_H
#define LEVEL_1_H

#include "baselevel.h"

class level_1 : public baseLevel
{
public:
	explicit level_1(QObject *parent = nullptr);
};

#endif // LEVEL_1_H
