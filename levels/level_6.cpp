#include "level_6.h"
#include "../entities/sentryentity.h"

void level_6::refreshint(updateinfo info)
{
	lever->reposition(info);
	lever2->reposition(info);
	lever3->reposition(info);
	gunner[0]->enableSpawn=!leverpressed;
	gunner[1]->enableSpawn=leverpressed;
	gunner[2]->enableSpawn=!leverpressed2;
	gunner[3]->enableSpawn=!leverpressed3;
}

level_6::level_6(QObject *parent)
	: baseLevel{parent, true}
{
	spawn->rpos={BLOCKHEIGHT*2, BLOCKHEIGHT*13};
	goal->rpos={BLOCKHEIGHT*90, BLOCKHEIGHT*13};

	texts.push_back(new textEntity(this, "\"Thank you for saving our asses, use your remote to maneuver this completly safe way to your price\"", {(float)BLOCKHEIGHT*2,(float)BLOCKHEIGHT*9}));

	texts.push_back(new textEntity(this, "\"Just go through this portal to get your well-deserved CAKE\"", {(float)BLOCKHEIGHT*82,(float)BLOCKHEIGHT*9}));

	for(int i=0; i<15; i++)
		blocks.push_back(new blockEntity(this, {(float)0,(float)i*BLOCKHEIGHT}, true));
	for(int i=0; i<101; i++)
		blocks.push_back(new blockEntity(this, {(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*15}, true));
	for(int i=0; i<15; i++)
		blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*100,(float)i*BLOCKHEIGHT}, true));
	for(int i=0; i<12; i++)
		blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*42,(float)i*BLOCKHEIGHT}, true));

	lever=new leverEntity(this, &leverpressed, {(float)BLOCKHEIGHT*26, (float)BLOCKHEIGHT*2});
	lever2=new leverEntity(this, &leverpressed2, {(float)BLOCKHEIGHT*55, (float)BLOCKHEIGHT*2});
	lever3=new leverEntity(this, &leverpressed3, {(float)BLOCKHEIGHT*85, (float)BLOCKHEIGHT*2});
	gunner.push_back(new sentryEntity(this, &damager,{(float)BLOCKHEIGHT*35,(float)BLOCKHEIGHT*13}, -1, 7, 0));
	gunner.push_back(new sentryEntity(this, &damager,{(float)BLOCKHEIGHT*55,(float)BLOCKHEIGHT*14}, 1, 5, -45));
	gunner.push_back(new sentryEntity(this, &damager,{(float)BLOCKHEIGHT*65,(float)BLOCKHEIGHT*14}, -1, 5, -45));
	gunner.push_back(new sentryEntity(this, &damager,{(float)BLOCKHEIGHT*75,(float)BLOCKHEIGHT*14}, 1, 5, -45));

	flames.push_back(new flameEntity(this, &damager,{(float)BLOCKHEIGHT*35,(float)BLOCKHEIGHT*10}, false));
	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*35,(float)BLOCKHEIGHT*9}, true));
	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*35,(float)BLOCKHEIGHT*11}, true));
	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*35,(float)BLOCKHEIGHT*12}, true));
	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*35,(float)BLOCKHEIGHT*14}, true));
	flames.push_back(new flameEntity(this, &damager,{(float)BLOCKHEIGHT*34,(float)BLOCKHEIGHT*10}, true));
	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*34,(float)BLOCKHEIGHT*9}, true));
	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*34,(float)BLOCKHEIGHT*11}, true));
	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*34,(float)BLOCKHEIGHT*12}, true));
	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*34,(float)BLOCKHEIGHT*13}, true));
	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*34,(float)BLOCKHEIGHT*14}, true));
	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*29,(float)BLOCKHEIGHT*13}, true));
	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*50,(float)BLOCKHEIGHT*12}, true));
	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*50,(float)BLOCKHEIGHT*13}, true));
	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*50,(float)BLOCKHEIGHT*14}, true));
	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*43,(float)BLOCKHEIGHT*11}, true));
	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*44,(float)BLOCKHEIGHT*11}, true));



	for(int i=0; i<gunner.length(); i++){
		damager.push_back(gunner[i]);
	}
	for(int i=0; i<blocks.length(); i++){
		colliders.push_back(blocks[i]);
	}

	nextlevel=level7;
	reSpawn();
}

level_6::~level_6()
{
	delete lever;
	delete lever2;
	delete lever3;
}
