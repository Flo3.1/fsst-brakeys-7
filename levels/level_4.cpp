#include "level_4.h"
#include "../entities/smithentity.h"

level_4::level_4(QObject *parent)
	: baseLevel{parent}
{
	spawn->rpos={BLOCKHEIGHT*2, BLOCKHEIGHT*13};
	goal->rpos={BLOCKHEIGHT*90, BLOCKHEIGHT*13};

	texts.push_back(new textEntity(this, "Precision is key", {(float)BLOCKHEIGHT*8,(float)BLOCKHEIGHT*9}));
	texts.push_back(new textEntity(this, "We got your back !\n                             !", {(float)BLOCKHEIGHT*10,(float)BLOCKHEIGHT*12.5}));

	for(int i=0; i<15; i++)
		blocks.push_back(new blockEntity(this, {(float)0,(float)i*BLOCKHEIGHT}));
	for(int i=0; i<30; i++)
		blocks.push_back(new blockEntity(this, {(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*15}));
	for(int i=80; i<101; i++)
		blocks.push_back(new blockEntity(this, {(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*15}));
	for(int i=0; i<15; i++)
		blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*100,(float)i*BLOCKHEIGHT}));
	for(int i=30; i<=80; i++)
		soups.push_back(new soupEntity(this, {(float)i*BLOCKHEIGHT,(float)BLOCKHEIGHT*15},true));

	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*24,(float)BLOCKHEIGHT*14}));

	gunner.push_back(new smithEntity(this, &damager,{(float)BLOCKHEIGHT*23,(float)BLOCKHEIGHT*13}, 1, 1, 0));
	gunner.push_back(new smithEntity(this, &damager,{(float)BLOCKHEIGHT*24,(float)BLOCKHEIGHT*12}, 1, 1, 0));


	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*40,(float)BLOCKHEIGHT*12}));
	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*50,(float)BLOCKHEIGHT*12}));
	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*60,(float)BLOCKHEIGHT*12}));
	blocks.push_back(new blockEntity(this, {(float)BLOCKHEIGHT*70,(float)BLOCKHEIGHT*12}));


	for(int i=0; i<gunner.length(); i++){
		damager.push_back(gunner[i]);
	}
	for(int i=0; i<blocks.length(); i++){
		colliders.push_back(blocks[i]);
	}
	nextlevel=level5;
	reSpawn();
}
