#ifndef LEVEL_7_H
#define LEVEL_7_H

#include "baselevel.h"

class level_7 : public baseLevel
{
	Q_OBJECT
public:
	QList<baseEntity*> notDamager;
	explicit level_7(QObject *parent = nullptr);

	void gametickint(updateinfo info);

signals:
	void credits();


};

#endif // LEVEL_7_H
