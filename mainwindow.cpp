#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)	: QMainWindow(parent)
{
	menu=new class menuWidget();
	setCentralWidget(menu);
	//int id;
	connect(menu, SIGNAL(startLevel(levelids)), this, SLOT(startLevel(levelids)));
}

MainWindow::~MainWindow()
{
	if (menu!=NULL)
		delete menu;
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
	//centralWidget()
}

void MainWindow::startLevel(levelids id)
{
	if(game==NULL){
		if(menu!=NULL){
			delete menu;
			menu=NULL;
		}
		game=new class gameWidget(id);
		setCentralWidget(game);
		connect(game, SIGNAL(credits()), this, SLOT(showCredits()));
	}
}

void MainWindow::mainMenu()
{
	if(menu==NULL){
		if(game!=NULL){
			delete game;
			game=NULL;
		}
		menu=new class menuWidget();
		setCentralWidget(menu);
		connect(menu, SIGNAL(startLevel()), this, SLOT(startLevel()));
	}
}

void MainWindow::showCredits()
{
	if(menu==NULL){
		if(game!=NULL){
			delete game;
			game=NULL;
		}
		menu=new class menuWidget(true);
		setCentralWidget(menu);
		connect(menu, SIGNAL(play()), this, SLOT(startLevel()));
	}
}

