#include "menuwidget.h"
#include <QFont>
#include <iostream>
#include <QCoreApplication>

menuWidget::menuWidget()
{

	intit();

	setScene(mainMenu);

	reposition();
}

menuWidget::menuWidget(bool credits)
{
	intit();
	setScene(cretidMenu);

	reposition();
}



menuWidget::~menuWidget()
{

	delete levelMen;
	delete mainMenu;
	delete cretidMenu;
}

void menuWidget::resizeEvent(QResizeEvent *event)
{
	// this is split into two methods so you can call reposition more easy
	reposition();
}

void menuWidget::intit()
{
	// all the signals and slots will stay conectet, even if the menu changes
	// this makes it that all the menus are loadet simultaniously
	// but it is easier to programm
	levelMen=new levelMenu();
	mainMenu=new mainMenuScene();
	cretidMenu=new creditScene();
	connect(mainMenu->textExit, SIGNAL(pressed()), this, SLOT(exit()));
	connect(mainMenu->textCredits, SIGNAL(pressed()), this, SLOT(credits()));
	connect(mainMenu->textPlay, SIGNAL(pressed()), this, SLOT(playlv1()));
	connect(mainMenu->textLevels, SIGNAL(pressed()), this, SLOT(levels()));

	connect(cretidMenu->textExit, SIGNAL(pressed()), this, SLOT(menu()));

	connect(levelMen->textExit, SIGNAL(pressed()), this, SLOT(menu()));
	connect(levelMen->textLevel[0], SIGNAL(pressed()), this, SLOT(playlv1()));
	connect(levelMen->textLevel[1], SIGNAL(pressed()), this, SLOT(playlv2()));
	connect(levelMen->textLevel[2], SIGNAL(pressed()), this, SLOT(playlv3()));
	connect(levelMen->textLevel[3], SIGNAL(pressed()), this, SLOT(playlv4()));
	connect(levelMen->textLevel[4], SIGNAL(pressed()), this, SLOT(playlv5()));
	connect(levelMen->textLevel[5], SIGNAL(pressed()), this, SLOT(playlv6()));
}

void menuWidget::reposition()
{
	int height=this->size().height()-3;
	int width=this->size().width()-3;
	scene()->setSceneRect(0,0,width, height); // for some reason this is nececary

	//resizes the Scene, because there is no resize event in the scene object
	//and using events would overcomplicate things
	((baseMenuScene*)scene())->resize();
}

void menuWidget::playlv1()
{
	emit startLevel(level1);
}

void menuWidget::playlv2()
{
	emit startLevel(level2);
}

void menuWidget::playlv3()
{
	emit startLevel(level3);
}

void menuWidget::playlv4()
{
	emit startLevel(level4);
}

void menuWidget::playlv5()
{
	emit startLevel(level5);
}

void menuWidget::playlv6()
{
	emit startLevel(level6);
}

void menuWidget::exit()
{
	QCoreApplication::exit();// tschau
}

void menuWidget::credits()
{
	setScene(cretidMenu);
	reposition();
}

void menuWidget::menu()
{
	setScene(mainMenu);
	reposition();
}

void menuWidget::levels()
{
	setScene(levelMen);
	reposition();
}
