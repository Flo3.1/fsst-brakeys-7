#include "updateinfo.h"

updateinfo::updateinfo(const double iheightpx, const double icamerapos, const double iwidhtunits)
{
	heightpx=iheightpx;
	camerapos=icamerapos;
	widhtunits=iwidhtunits;
}

updateinfo::updateinfo(const double info[3]){
	heightpx=info[0];
	camerapos=info[1];
	widhtunits=info[2];

}

updateinfo::updateinfo(const updateinfo &info)
{
	heightpx=info.heightpx;
	camerapos=info.camerapos;
	widhtunits=info.widhtunits;
}

updateinfo updateinfo::operator=(const updateinfo &info)
{
	heightpx=info.heightpx;
	camerapos=info.camerapos;
	widhtunits=info.widhtunits;
	return *this;
}
